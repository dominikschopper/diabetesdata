var gulp = require('gulp');
var less = require('gulp-less');
var path = require('path');

gulp.task('default', function() {
	console.log('running the default task');
});


gulp.task('less2css', function () {
	gulp.src('./less/*.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'css') ]
    }))
    .pipe(gulp.dest('./docroot/css'));
});
