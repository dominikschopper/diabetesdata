/**
 * connects to db and returns the Schema for our connection
 * @version 0.1
 * @author Dominik Schopper
 */


var Schema;
var mongoose = require('mongoose');
var db;

mongoose.set('debug', true);

(function () {
	"use strict";
	//var mongoose = require('mongoose');

	if (!db) {
		db = mongoose.connect('mongodb://localhost/DiabetesData');
	}
	Schema = mongoose.Schema;
}())
 
module.exports =  Schema;
 