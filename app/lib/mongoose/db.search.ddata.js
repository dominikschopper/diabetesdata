/**
 * @version 0.1
 * @author Dominik Schopper
 */
"use strict";

/**
 * @param {Date|String}	from	date from when on data should be displayed
 * @param {Date|String}	to		until which date data should be displayed
 * @param {String}		userid	for which userid
 * @param {Function}	cb		a callback function to be given to the mongoose.model.find(<cb>)
 */
function searchDData(from, to, userid, cb) {
	var DdataSchema = require('./Schemas/Ddata.js'),
		mongoose = require('mongoose'),
		data,
		result;

	console.log('SEARCH parameters from:"', from, '" to:"', to, '" userid:"', userid,'"');

	if (!userid) {
		userid = 'mail@dschopper.de';
	}

	if (!from) {
		from = new Date();
		from.setDate(from.getDate() - 7);
		from.setHours(0);
		from.setMinutes(0);
	} else if (from.constructor !== Date) {
		from = new Date(from);
	}

	data = mongoose.model("data", DdataSchema);

	if (!to) {
		result = data.find({owner: userid}).where("date").gte(from).sort('-date').exec(cb);
	} else {
		if (typeof to === 'string') {
			to = new Date(to);
		}
		result = data.find({owner: userid}).where("date").gte(from).lte(to).sort('-date').exec(cb);
	}

	console.log('SEARCH queries from:"', from, '" to:"', to, '" userid:"', userid,'"');
	return result;

}

module.exports = searchDData;
