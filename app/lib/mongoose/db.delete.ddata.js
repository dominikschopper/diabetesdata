/**
 * @version 0.1
 * @author Dominik Schopper
 */
"use strict";

function deleteData(uid, cb) {
	
    try {
        console.log('require delete');

        var mongoose = require('mongoose');
        var DdataSchema = require('./Schemas/Ddata.js');
        var DData = mongoose.model("data", DdataSchema);
        //var d = new DData(data);
        DData.findOneAndRemove({ _id: uid }, null, cb);

    } catch (e) {
        console.log('error while loading db.inser.ddata', e);
    }
    return;
}

module.exports = deleteData;