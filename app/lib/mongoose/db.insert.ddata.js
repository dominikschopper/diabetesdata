/**
 * @version 0.1
 * @author Dominik Schopper
 */
"use strict";

function insertData(data, cb) {
    try {
        console.log('require insert');

        var mongoose = require('mongoose');
        var DdataSchema = require('./Schemas/Ddata.js');
        var DData = mongoose.model("data", DdataSchema);
        var d = new DData(data);
        d.save(cb);

    } catch (e) {
        console.log('error while loading db.inser.ddata', e);
    }
    return;
}

module.exports = insertData;