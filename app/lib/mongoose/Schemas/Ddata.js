/**
 * @version 0.1
 * @author Dominik Schopper
 */




var Schema = require('../../db.Schema.js');
//console.log('Schema',Schema);
//var cfg = require('app/config.js');
/*
{
    "type": "insulin",
    "measurement": "ie",
    "name": "Humalog",
    "date": "2014-04-29 12:00",
    "value": 6,
    "note": "Dosis 1:1",
    "owner": "mail@dschopper.de"
  },
*/

module.exports = new Schema(
	{
		type:  { type: String, index: true },
		measurement: String,
		name: String,
		date: { type: Date, index: true },
		value: Number,
		note: String,
		owner: { type: String, index: true }
	},
	{
		collection: "data",
		autoIndex: true
	}
);
