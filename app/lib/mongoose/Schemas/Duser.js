/**
 * @version 0.1
 * @author Dominik Schopper
 */



	var DUserSchema = require('./mongoose/Schemas/Duser.js');
//var DDataSchema = require('./mongoose/Schemas/Ddata.js');


//db.on('error', console.error.bind(console, 'connection error:'));
//db.once('open', function callback () {
//	console.log("we're connected");
//});


//console.log('data schema:', DDataSchema);
/*
try {

	var UserModel = mongoose.model('users', DUserSchema);
	var user = new UserModel({
		"type": "local",
		"name": "test@test.com",
		"password": "c2x:e6b00e8a57d538953ee9747276cdd81c", // with "c2x" salted md5 of "123qwe"
		configuration: {
			insulin: [
			{
				"name": "Insulin 01",
				"color": "rgba(192,92,92, 0.7)"
			},
			{
				"name": "Second Insulin",
				"color": "#dadada"
			}
			],
			enter: {
				"default": "meal"
			}
		},
		searchable: true
	});

	user.save(function (err, user) {
		if (err)  {
			return console.error(err);
		}
		console.log(user);
	});

} catch (e) {
	console.log(e);
}

});

*/
/*
{
  "type": "local",
  "name": "mail@dschopper.de",
  "password": "c2x:e6b00e8a57d538953ee9747276cdd81c", // with "c2x" salted md5 of "123qwe"
  configuration: {
    insulin: [
      {
        "name": "Humalog",
        "color": "yellow"
      },
      {
        "name": "Lantus",
        "color": "#DADBDC"
      }
    ],
    enter: {
      "default": "sugar"
    }
  },
  searchable: true,
  created: "2014-04-30 12:11:09",
  changed:  "2014-04-30 14:17:33"
}
*/

module.exports = new Schema(
	{
		type: String,
		name: String,
		password: String,
		configuration: {
			insulin: [
				{
					name: String,
					color: String
				}
			],
			measurements: {
				insulin: String,
				meal: String,
				sugar: String
			},
			enter: {
				default: String
			}
		},
		searchable: Boolean,
		created: {
			type: Date,
			default: Date.now 
		},
		changed:  {
			type: Date,
			default: Date.now
		}
	},
	{ 
		autoIndex: true 
	}
);
