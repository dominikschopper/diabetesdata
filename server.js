/**
 * main script for diabetesdata application
 *
 * @version 0.1
 * @author <dev@dschopper.de>
 */
"use strict";

// get the bi server
var server = require('bi-appsrvr');

// returns a sugered version of express w. static document handling in documentRoot
var app = server.run({
    "httpServer": {
        "port": 8088,
        "host": "taka-tuka.net"
    },
    "httpStatic": {
        "documentRoot": "docroot"
    },
    "session": {
        "secret": "123+qWe#AsD"
    }/*,
    "views": {
    	"folder": "app/views/",
    	"engines": [
    		{ 'doT': require('dot') }
    	]
    }*/
});
/*
app.get('/data', function(req, res, next) {
    var fs = require('fs');
    fs.readFile('temp/data.json', function(err, data) {
        res.set('Content-Type', 'application/json');
        if (err) {
            res.end({
                value: "file not found"
            });
        }
        var rawData = JSON.parse(data);
        res.end(JSON.stringify(rawData));
        return;
    });
    return;
});
*/
// registered
app.get('/login', function(req, res, next) {
    var username = (req.query.username ? req.query.username : 'nobody@taka-tuka.net');
    var passwordHash = req.query.password;
    var userCheckOk = false;
    try {
        if (!req.signedCookies.userid || req.query.refresh) {
            res.cookie('userid', username, {
                maxAge: (1000 * 3600 * 24 * 30)
            });
        }
    } catch (e) {
        console.log(e);
    }
    console.log('username/userid', username);
    res.set('Content-Type', 'text/plain');
    res.send('login test');
    // next();
    return;
});

/** ===== DELETEING DATA ===== */
app.delete('/data/:uid', function (req, res, next) {

		console.log('starting delete');

		var deleteCallback = function (err) {
			if (err) {
				throw new Error('cannot delete from mongodb: >>'+ err +'<<');
			}
			res.send('{"error":false,"message":"object deleted"}');
		};
try {
		var uid = req.params.uid,
			userid = (req.cookies.userid ? req.cookies.userid : 'mail@dschopper.de'),
			bounce = (req.headers.referer ? req.headers.referer : '/list.html'),
			delFunc = require('./app/lib/mongoose/db.delete.ddata.js');

		console.log('trying to delete', uid);

		if (!uid) {
			res.redirect(bounce);
			return;
		}

		if (!userid) {
			res.redirect('/login');
			return;
		}

		delFunc(uid, deleteCallback)
} catch (e) {
	console.log('error while deleting', e);
	return;
}
//		next();
		return;
});

/** ===== INSERTING DATA ===== */
app.post('/data', function(req, res, next) {
    try {

        var insert = require('./app/lib/mongoose/db.insert.ddata.js'),
        	userid = (req.cookies && req.cookies.userid ? req.cookies.userid : 'mail@dschopper.de'),
			tz = (req.body["timezone-offset"] ? (req.body["timezone-offset"] * 1) : 0),
			measurement = (req.body.measurement ? req.body.measurement : ''),
        	obj = {
            	owner: userid,
            	measurement: measurement
        	},
        	keys = ['type', 'value', 'date', 'note', 'name'],
        	key = '';

/*		console.log('request.body', req.body);
		console.log('req.signedCookies', req.cookies);
		console.log('timezone offset', tz);
  */

        if (!userid) {
            res.cookie('userid', 'mail@dschopper.de', {
                maxAge: (1000 * 3600 * 24 * 30)
            });
            //res.redirect('/login');
            //return;
        }
        res.set('Content-Type', 'text/plain');
//        console.log(req.body);
        for (var i = 0; i < keys.length; i += 1) {
            key = keys[i];
            if (req.body.hasOwnProperty(key) && req.body[key]) {
                obj[key] = req.body[key];
            } else {
                obj[key] = '';
            }
        }
        if (req.body.uid) {
            obj._id = req.body.uid;
        }
//        console.log('raw date/time', req.body.date, req.body.time);
        obj.date = new Date(Date.parse(obj.date + ' ' + req.body.time + ':00'));
        obj.date.setHours(obj.date.getHours() + tz);
//        console.log('created obj', obj);
        insert(obj, function(err) {
            if (err) {
                console.log('error inserting/updating mongodb data collection', err);
                throw new Error('Cannot insert into mongodb: >>' + err + '<<');
            }
            res.redirect('/enter.html');
            return;
        });
    } catch (e) {
        console.log('insert/update callback threw an error', err);
        res.redirect('/enter.html');
        return;
    }
    return;
});
/** ===== QUERYING DATA ===== */
app.get('/data', function(req, res, next) {

    console.log('were querying data!');

    /** callback function for a mongoose promise */
    var parseResult = function (err, data) {
        if (err) {
            throw new Error('database Error >>' + err + '<<');
        }
        //		res.set('Content-Type', 'application/json');
        res.set('Content-Type', 'text/plain');
        res.end(JSON.stringify(data));
        return;
    };
    var from = (req.query.from ? new Date(req.query.from) : false),
        to = (req.query.to ? new Date(req.query.to) : false),
        userid = (req.signedCookies.userid ? req.signedCookies.userid : undefined);
    console.log('req.signedCookies', req.cookies);
    try {
    	if (!from) {
    		from = new Date();
    		from.setDate(from.getDate()-7);
    		from.setHours(0);
    		from.setMinutes(0);
    		from.setSeconds(0);
    	}
        var searchDData = require('./app/lib/mongoose/db.search.ddata');
        res.set('Cache-Control', 'no-cache');
        res.set('ETag', Date.now());
        searchDData(from, to, userid, parseResult);
    } catch (e) {
        console.log('CATCHING ERROR', e);
        return;
    }
    return;
});

/** ===== STATIC CONTENT ===== */
app.get('/', function(req, res, next) {
    console.log(req.route);
    // static stuff
    next();
    return;
});

/** ===== TESTING VIEWS ===== */
app.get('view-test', function (req, res, next()) {
	;
});