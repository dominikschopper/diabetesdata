/**
 * @version 0.1
 * @author <dev@dschopper.de>
 */
"use strict";
require(['DDataObject', 'DDataConfig', 'toolkit'], function(DDataObject, conf, tk) { // ----- SCOPE -----
	var ttt = [];
	function prepareDayData(data) {
		var prepData = [],
			i=0,
			actDate,
			meDate,
			actDateKey,
			daySeries = [],
			days = {},
			red = 124,
			dateFormat = d3.time.format("%d.%m.%Y"),
			o = {};

		for (i=0; i<data.length; i+=1) {
			if (data[i].type === 'sugar') {

				//console.log(data[i].date);

				actDate = new Date(data[i].date);
				actDateKey =  dateFormat(actDate);

				meDate = new Date();
				meDate.setHours(actDate.getHours());
				meDate.setMinutes(actDate.getMinutes());
				meDate.setSeconds(actDate.getSeconds());
				//console.log(actDate);

				if (!days.hasOwnProperty(actDateKey)) {
					days[actDateKey] = [];
				}
				o = {
					x: meDate,
					y: (data[i].hasOwnProperty('value') ? data[i].value : 0) 
				};
				days[actDateKey].push(o);
				ttt.push(meDate);
				o = {};
			}
		}

		for (i in days) {
			if (days.hasOwnProperty(i)) {

				//days[i].sort(function (a, b) { return +a[0] - +b[0]; })

				prepData.push({
					key: "Blutzucker "+ i,
					values: days[i]
				});
			}
		}

		console.log('testData', prepData);

		return prepData;
	}

	function prepareData(data) {
		var sugarSeries = [ ],
			insulinSeries = [ ],
			mealSeries = [ ],
			activitySeries = [ ],
			i = 0,
			minTime,
			maxTime;
		minTime = new Date(data[0].date).getTime();
		for (i=0; i<data.length; i+=1) {
			if (data[i].type === 'sugar') {
				sugarSeries.push({
					x: new Date(data[i].date),
					y: data[i].value
				});
				maxTime = data[i].date
			} else if (data[i].type === 'meal') {
				mealSeries.push({
					x: new Date(data[i].date),
					y: data[i].value
				});
			} else if (data[i].type === 'insulin' && data[i].name === 'Humalog') {
				insulinSeries.push({
					x: new Date(data[i].date),
					y: data[i].value
				});
				maxTime = data[i].date
			}
		}
		maxTime = new Date(maxTime).getTime();

		return [
			{
				key: "Blutzucker",
				values: sugarSeries,
				color: "#C26e6e"
			},
			{
				key: "Humalog",
				bar: true,
				values: insulinSeries,
				color: "#4584DA"
			}/*,
			{
				key: "Essen",
				bar: true,
				values: mealSeries,
				color: "#45DA84"
			}*/
		];
	}


	$.ajax({
		url: '/data/',
		dataType: 'json'
	})
	.done(function (data) {
		data.sort(function (a, b){
			return new Date(a.date) - new Date(b.date);
		})
		//drawChart(data);
		drawDayChart(data);
	})
	.fail(function (xhr) {
		console.log('fail', xhr);
		alert('data request failed');
	});

/*
    var testdata = [{
        "key": "Quantity",
        "bar": true,
        "values": [
            [1136005200000, 1271000.0],
            [1138683600000, 1271000.0],
            [1141102800000, 1271000.0],
            [1143781200000, 0],
            [1146369600000, 0],
            [1149048000000, 0],
            [1151640000000, 0],
            [1154318400000, 0],
            [1156996800000, 0],
            [1159588800000, 3899486.0],
            [1162270800000, 3899486.0],
            [1164862800000, 3899486.0],
            [1167541200000, 3564700.0],
            [1170219600000, 3564700.0],
            [1172638800000, 3564700.0],
            [1175313600000, 2648493.0],
            [1177905600000, 2648493.0],
            [1180584000000, 2648493.0],
            [1183176000000, 2522993.0],
            [1185854400000, 2522993.0],
            [1188532800000, 2522993.0],
            [1191124800000, 2906501.0],
            [1193803200000, 2906501.0],
            [1196398800000, 2906501.0],
            [1199077200000, 2206761.0],
            [1201755600000, 2206761.0],
            [1204261200000, 2206761.0],
            [1206936000000, 2287726.0],
            [1209528000000, 2287726.0],
            [1212206400000, 2287726.0],
            [1214798400000, 2732646.0],
            [1217476800000, 2732646.0],
            [1220155200000, 2732646.0],
            [1222747200000, 2599196.0],
            [1225425600000, 2599196.0],
            [1228021200000, 2599196.0],
            [1230699600000, 1924387.0],
            [1233378000000, 1924387.0],
            [1235797200000, 1924387.0],
            [1238472000000, 1756311.0],
            [1241064000000, 1756311.0],
            [1243742400000, 1756311.0],
            [1246334400000, 1743470.0],
            [1249012800000, 1743470.0],
            [1251691200000, 1743470.0],
            [1254283200000, 1519010.0],
            [1256961600000, 1519010.0],
            [1259557200000, 1519010.0],
            [1262235600000, 1591444.0],
            [1264914000000, 1591444.0],
            [1267333200000, 1591444.0],
            [1270008000000, 1543784.0],
            [1272600000000, 1543784.0],
            [1275278400000, 1543784.0],
            [1277870400000, 1309915.0],
            [1280548800000, 1309915.0],
            [1283227200000, 1309915.0],
            [1285819200000, 1331875.0],
            [1288497600000, 1331875.0],
            [1291093200000, 1331875.0],
            [1293771600000, 1331875.0],
            [1296450000000, 1154695.0],
            [1298869200000, 1154695.0],
            [1301544000000, 1194025.0],
            [1304136000000, 1194025.0],
            [1306814400000, 1194025.0],
            [1309406400000, 1194025.0],
            [1312084800000, 1194025.0],
            [1314763200000, 1244525.0],
            [1317355200000, 475000.0],
            [1320033600000, 475000.0],
            [1322629200000, 475000.0],
            [1325307600000, 690033.0],
            [1327986000000, 690033.0],
            [1330491600000, 690033.0],
            [1333166400000, 514733.0],
            [1335758400000, 514733.0]
        ]
    }, {
        "key": "Price",
        "values": [
            [1136005200000, 71.89],
            [1138683600000, 75.51],
            [1141102800000, 68.49],
            [1143781200000, 62.72],
            [1146369600000, 70.39],
            [1149048000000, 59.77],
            [1151640000000, 57.27],
            [1154318400000, 67.96],
            [1156996800000, 67.85],
            [1159588800000, 76.98],
            [1162270800000, 81.08],
            [1164862800000, 91.66],
            [1167541200000, 84.84],
            [1170219600000, 85.73],
            [1172638800000, 84.61],
            [1175313600000, 92.91],
            [1177905600000, 99.8],
            [1180584000000, 121.191],
            [1183176000000, 122.04],
            [1185854400000, 131.76],
            [1188532800000, 138.48],
            [1191124800000, 153.47],
            [1193803200000, 189.95],
            [1196398800000, 182.22],
            [1199077200000, 198.08],
            [1201755600000, 135.36],
            [1204261200000, 125.02],
            [1206936000000, 143.5],
            [1209528000000, 173.95],
            [1212206400000, 188.75],
            [1214798400000, 167.44],
            [1217476800000, 158.95],
            [1220155200000, 169.53],
            [1222747200000, 113.66],
            [1225425600000, 107.59],
            [1228021200000, 92.67],
            [1230699600000, 85.35],
            [1233378000000, 90.13],
            [1235797200000, 89.31],
            [1238472000000, 105.12],
            [1241064000000, 125.83],
            [1243742400000, 135.81],
            [1246334400000, 142.43],
            [1249012800000, 163.39],
            [1251691200000, 168.21],
            [1254283200000, 185.35],
            [1256961600000, 188.5],
            [1259557200000, 199.91],
            [1262235600000, 210.732],
            [1264914000000, 192.063],
            [1267333200000, 204.62],
            [1270008000000, 235.0],
            [1272600000000, 261.09],
            [1275278400000, 256.88],
            [1277870400000, 251.53],
            [1280548800000, 257.25],
            [1283227200000, 243.1],
            [1285819200000, 283.75],
            [1288497600000, 300.98],
            [1291093200000, 311.15],
            [1293771600000, 322.56],
            [1296450000000, 339.32],
            [1298869200000, 353.21],
            [1301544000000, 348.5075],
            [1304136000000, 350.13],
            [1306814400000, 347.83],
            [1309406400000, 335.67],
            [1312084800000, 390.48],
            [1314763200000, 384.83],
            [1317355200000, 381.32],
            [1320033600000, 404.78],
            [1322629200000, 382.2],
            [1325307600000, 405.0],
            [1327986000000, 456.48],
            [1330491600000, 542.44],
            [1333166400000, 599.55],
            [1335758400000, 583.98]
        ]
    }].map(function(series) {
        series.values = series.values.map(function(d) {
            return {
                x: d[0],
                y: d[1]
            }
        });
        return series;
    });
    / *
//For testing single data point
var testdata = [
  {
    "key" : "Quantity" ,
    "bar": true,
    "values" : [ [ 1136005200000 , 1271000.0] ]
  } ,
  {
    "key" : "Price" ,
    "values" : [ [ 1136005200000 , 71.89] ]
  }
].map(function(series) {
  series.values = series.values.map(function(d) { return {x: d[0], y: d[1] } });
  return series;
});
*/


	function drawDayChart(data) {
		var testdata = prepareDayData(data);
	    var chart;
	    var sugarTickValues = (function (){
	    	var v = [];
	    	for (var i=0;i<400;i+=25) {
	    		v.push(i)
	    	};
	    	return v; 
	    }());
	    var timeValues = [ ];
//		console.log('sTValues', sugarTickValues);
	    
/*	    for (var t=0; t<24; t+=1) {
	    	var d = new Date(Date.now());
	    	d.setHours(t);
	    	d.setMinutes(0);
	    	d.setSeconds(0);
	    	timeValues.push(d);
	    }
*/
		for (var t=0; t<ttt.length; t+=1) {
			//console.log('time', d);
	    	timeValues.push(ttt[t]);
	    }
	    console.log('data series', testdata);

	    nv.addGraph(function() {
	        chart = nv.models.lineChart().margin({
	            top: 30,
	            right: 60,
	            bottom: 50,
	            left: 70
	        }).x(function(d, i) {
	            return d.x;
	        }).y(function (d) {
	        	return d.y;
	        }).useInteractiveGuideline(true)
            .color(d3.scale.category20().range());

	        chart.xAxis.tickFormat(function (d) { 
		        	//console.log('tickformat param', d)
		        	return d3.time.format("%H:%M")(new Date(d));
		        }).showMaxMin(true);

	        chart.yAxis
	        	.ticks(sugarTickValues)
	        	.tickFormat(d3.format("d"))
	        	.showMaxMin(true);

	        d3.select('#chart2 svg')
	        	.datum(testdata)
	        	.transition()
	        	.duration(700)
	        	.call(chart);

	        nv.utils.windowResize(chart.update);
	        chart.dispatch.on('stateChange', function(e) {
	            nv.log('New State:', JSON.stringify(e));
	        });
	        return chart;
	    });
	}



	function drawChart(data) {
		var testdata = prepareData(data);
	    var chart;
	    var sugarTickValues = (function (){var v = []; for (var i=0;i<400;i+=25) {v.push(i)}; return v; }());
	    //console.log('sTValues', sugarTickValues);
	    
	    nv.addGraph(function() {
	        chart = nv.models.linePlusBarChart().margin({
	            top: 30,
	            right: 60,
	            bottom: 50,
	            left: 70
	        }).x(function(d, i) {
	            return i
	        }).color(d3.scale.category10().range());
	        
	        chart.xAxis
		        	.tickFormat(function(d) {
			            var dx = testdata[0].values[d] && testdata[0].values[d].x || 0;
			            return dx ? d3.time.format('%d.%m %H:%M')(new Date(dx)) : '';
			        })
	        	.showMaxMin(true);
	        chart.y1Axis
	        	.tickFormat(function(d) {
		            return d3.format('d')(d) + ' ie';
		        })
		        .showMaxMin(true);
	        chart.y2Axis
	        	.tickValues(sugarTickValues)
	        	.tickFormat(function (d) { return d3.format('d')(d) +' mg/dl'; })
	        	.showMaxMin(true);
	        //chart.showControls(true);
	        //console.log(chart);
	        /*.y3Axis
	        	.tickValues([ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 ])
	        	.tickFormat(d3.format('d'))*/

	        //chart.bars.forceY([0]).padData(false);

	        //chart.lines.forceY([0]);
	        
	        d3.select('#chart1 svg').datum(testdata).transition().duration(700).call(chart);
	        nv.utils.windowResize(chart.update);
	        chart.dispatch.on('stateChange', function(e) {
	            nv.log('New State:', JSON.stringify(e));
	        });
	        return chart;
	    });
	}
}); // ----- /SCOPE -----