/**
 * @version 0.1
 * @author <dev@dschopper.de>
 */

require(['DDataObject', 'DDataConfig', 'js/vendor/nv.d3.js', 'js/vendor/d3.v3.js' ], function (DDataObject, conf, nv, d3) {	// ----- SCOPE -----
	
console.log(d3);

	var coll,
		obj = new DDataObject(),
		chartParentId = '#ddata-chart',
		maxTime = 0,
		minTime = 0;

	// console.log(conf);

	function prepareData(data) {
		var sugarSeries = [ ],
			insulinSeries = [ ],
			i = 0;
		minTime = new Date(data[0].date).getTime();
		for (i=0; i<data.length; i+=1) {
			if (data[i].type === 'sugar') {
				sugarSeries.push({
					x: new Date(data[i].date).getTime(),
					y: data[i].value
				});
				maxTime = data[i].date
			} else if (data[i].type === 'insulin' && data[i].name === 'Humalog') {
				insulinSeries.push({
					x: new Date(data[i].date).getTime(),
					y: data[i].value
				});
				maxTime = data[i].date
			}
		}
		maxTime = new Date(maxTime).getTime();

		return [
			{
				key: "Blutzucker",
				values: sugarSeries,
				color: "#f2dede"
			}/*,
			{
				key: "Humalog",
				values: insulinSeries,
				color: "#4584DA"
			}*/
		];
	}

	function drawChart(data) {
		nv.addGraph(function() {

			var chart = nv.models.lineChart();

			var minDate = new Date(minTime);
			var tempDate = new Date();

			tempDate.setFullYear(minDate.getFullYear())
			tempDate.setMonth(minDate.getMonth())
			tempDate.setDate(minDate.getDate())
			tempDate.setHours(minDate.getHours())
			tempDate.setMinutes(0);	

			timeValues = [];
			for (var i=+tempDate; i<=maxTime; i+=1800000) {
				timeValues.push(i)
			}

			chart.xAxis
				.orient('bottom')
				.axisLabel("Tag/Uhrzeit")
				.tickValues(timeValues)
				.tickFormat(
					d3.time.format.multi([
						["%d.%m.%Y", function(d) { return (d.getHours() === 0 && d.getMinutes() === 0); }],
						["%H:%i", function(d) { return (d.getMinutes() % 30 === 0); }]
					])
				);

			chart.yAxis
				.orient('left')
				.axisLabel("Zucker")
				.tickValues([ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 , 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34 ].map(function(v) {return v*10;}));

			d3.select("#ddata-chart")
				.datum(prepareData(data))
				.transition().duration(500).call(chart);

			nv.utils.windowResize(
					function() {
						chart.update();
					}
				);

			return chart;
		});
	}

	$.ajax({
		url: '/data/',
		dataType: 'json'
	})
	.done(function (data) {
		data.sort(function (a, b){
			return new Date(a.date) - new Date(b.date);
		})
		drawChart(data);
	})
	.fail(function (xhr) {
		console.log('fail', xhr);
		alert('data request failed');
	});

});				 // ----- /SCOPE -----
