/**
 *
 */

define([], function (){

	var leadingZeros = function (num, len) {
		len = len || 2;
		num = num +'';
		while (num.length < len) {
			num = '0'+ num;
		}
		return num;
	};

	return {
		leadingZeros: leadingZeros
	};
});