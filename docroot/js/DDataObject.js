/**
 * @version 0.1
 * @author <dev@dschopper.de>
 */

define(['toolkit'], function (tk) {	// ------- SCOPE -------
	"use strict";

	/* var cfg = {
		"insulin": {
			"Lantus": {
				"color": "rgba(121, 136, 212, 0.9)"
			},
			"Humalog": {
				"color": "#b9def0"
			}
		}
	}; */
//	console.log('cfg', conf);

	/**
	 * @class DDataObject
	 * @property {Object} data
	 * @property {String} type
	 */
	var DDataObject = function (data) {
		this.data = { };
		this.date = null;
		this.setData(data);
	};

	/**
	 * @memberOf {DDataObject}
	 */
	DDataObject.prototype.setData = function (data) {
		if (data) {
			this.data = data;
			this.date = new Date(this.data.date);
		}
		return this;
	};

	/**
	 * @memberOf {DDataObject}
	 */
	DDataObject.prototype.getDDataObject = function () {
		return this.data;
	};

	/**
	 * @memberOf {DDataObject}
	 */
	DDataObject.prototype.getDateObject = function () {
		return this.date;
	};

	/**
	 * @memberOf {DDataObject}
	 */
	DDataObject.prototype.getDateString = function () {
		return tk.leadingZeros(this.date.getDate()) +'.'+ 
			tk.leadingZeros((this.date.getMonth() + 1));
	};

	DDataObject.prototype.getYear = function () {
		//console.log('called getYear', this.date.getFullYear());
		return this.date.getFullYear();
	};

	/**
	 * @memberOf {DDataObject}
	 */
	DDataObject.prototype.getTimeString = function () {
		var h = tk.leadingZeros(this.date.getHours());
		var m = tk.leadingZeros(this.date.getMinutes());

		return h +':'+  m;
	};

	/**
	 * @memberOf {DDataObject}
	 */
	DDataObject.prototype.getType = function () {
		return this.data.type;
	};

	/**
	 * @memberOf {DDataObject}
	 */
	DDataObject.prototype.getValue = function () {
		return this.data.value;
	};

	/**
	 * @memberOf {DDataObject}
	 */
	DDataObject.prototype.getMeasurement = function () {
		if (this.data.hasOwnProperty('measurement')) {
			return this.data.measurement;
		}
		return '';
	};

	/**
	 * @memberOf {DDataObject}
	 */
	DDataObject.prototype.getNote = function () {
		if (this.data.hasOwnProperty('note')) {
			return this.data.note;
		}
		return '';
	};

	/**
	 * @memberOf {DDataObject}
	 */
	DDataObject.prototype.getInsulin = function () {
		return (this.data.hasOwnProperty('name') ? this.data.name : '');
	};

	DDataObject.prototype.getColor = function (cf) {
		// console.log('!!!cfiguration!!', cf);
		if (this.getType() === 'insulin') {
			var key = this.getInsulin();
			if (cf && cf.hasOwnProperty('insulin') && cf.insulin.hasOwnProperty(key) && cf.insulin[key].hasOwnProperty('color')) {
				return cf.insulin[key].color;
			}
			return '#FAFAFA';
		}
		return '';
	};

	return DDataObject;
});			// ----- /SCOPE -----
