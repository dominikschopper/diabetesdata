/**
 * @version 0.1
 * @author <dev@dschopper.de>
 */

require(['DDataObject', 'DDataConfig', 'toolkit', 'js/vendor/doT.js' ], function (DDataObject, conf, tk, dot) {	// ----- SCOPE -----
	
	var coll,
		obj = new DDataObject(),
		tableParentId = '#table-wrapper';

	// console.log(conf);

	function loadData() {

		$.ajax({
			url: '/data',
			dataType: 'json'
		})
		.done(function (data) {
			
			var tableWrapper = $(tableParentId);
			tableWrapper.children().hide(400);
			tableWrapper.empty().children().show(600)
		
			drawTable(data, tableWrapper);
		})
		.fail(function (xhr) {
			console.log('fail', xhr);
			alert('data request failed');
		});
	}



	function drawTable(data, where) {
		
//		console.log('complete data', data);
		
		var tableTempl = dot.template( $('#data-table').text() );
		var dataTempl = dot.template( $('#data-row').text() );
		var dateTempl = dot.template($('#date-row').text());
		
		var tableData = '',     // stores the table rows
			table = '',         // stores the whole table
			insulin = '',       // insulin info
			insBgColor = '',	// individual bgcolor for each insulin type
			myDate = '',        // date (the actual)
			lastDate = '';      // the last date shown in the html table

		
		// sort desc by date attribute
		data.sort(function (a, b) {
			var aDate, bDate;
			if (b.date.constructor === Date) {
				bDate = b.date;
			} else {
				bDate = new Date(b.date);
			}
			if (a.date.constructor === Date) {
				aDate = a.date;
			} else {
				aDate = new Date(a.date);
			}
			return bDate - aDate; 
		});

		for (var i=0; i < data.length; i+=1) {

			lastDate = myDate;
			insBgColor = '';

			obj.setData(data[i]);

			if (obj.getType() === 'insulin') {
				insulin = obj.getInsulin();
				insBgColor = 'background-color:'+ obj.getColor(conf) +';';
			} else {
				insulin = '';
			}

			myDate = obj.getDateString();
			if (myDate !== lastDate) {
				tableData += dateTempl({
					date: myDate,
					year: obj.getYear()
				});
			}
//			console.log(obj);
			tableData += dataTempl({
				html: { backgroundColor: insBgColor },
				date: '',
				time: obj.getTimeString(),
				type: obj.getType(),
				insulin: insulin,
				value: obj.getValue() + (obj.getMeasurement() ? ' '+ obj.getMeasurement() : ''),
				note: obj.getNote(),
				uid: obj.data._id
			});

		}

		table = tableTempl({
			html: {
				id: "data-table",
				classes: "data-table"
			},
			header: {
				date: 'Datum',
				time: 'Uhrzeit',
				type: 'Was',
				value: 'Wert',
				note: 'Bemerkung'
			},
			data: tableData
		});

		$(where).empty().append(table);
		$(where).append('<div class="clearfix"></div>');
	};

	function showWarningDialog(uid, title, message) {
		var dialogElement = $($('#dialog-wrapper .dialog-content').clone());
		dialogElement.append("<h5>"+ title +"</h5>").append("<p>"+ message +"</p>");
		dialogElement.dialog({
			modal: true,
			buttons: [ 
				{
					text: "Löschen",
					click: function() {
						deleteRow()
						$(this).dialog("close"); 
					} 
				} 
			]
		});
	};

	function filterData() {
		var className = '.show-filter';
		var elements = $(className);
		elements.each(function (idx, el) {
			var $el = $(el);
			// console.log('checked #'+ idx, $el.attr('checked'));
			//console.log('showing/hiding #'+ idx,$('#data-table').find('.'+ $(el).data('type') +'-row'));
			if (el.checked) {
				$('#data-table').find('.'+ $(el).data('type') +'-row').show();
			} else {
				$('#data-table').find('.'+ $(el).data('type') +'-row').hide();
			}
		});
	}

	$('.show-filter').bind('change', function (ev) {
		window.setTimeout(filterData, 100);
	});

	$('.refresh-data').bind('click', function (ev) {
		loadData();
	});

	loadData();

//	console.log('delete-links', $('.delete-link'));

	$('#table-wrapper').bind('click', function (ev) {
		
		console.log('triggered click', ev.target);
		ev.preventDefault();

		var $link = $(ev.target).parent();

/*		if (!$link.hasClass('delete-link')) {
			console.log('searching for link');
			$link = $link.parents('.delete-link');
		}
*/
		console.log('link is %o', $link.get(0));

		if (!$link.hasClass('delete-link')) {
			return false;
		}

		var url = $link.attr('href');
		var row = $link.parents('.ddata-row');

console.log('deleting', url);

		$.ajax({
			type: "delete",
			url: url,
			dataType: 'json'
		})
		.done(function (ok) {
			row.hide(400).remove();
		})
		.fail(function (xhr) {
			console.log('fail', xhr);
			alert('delete request failed');
		});

	});

	var showHide = $('.showhide');
	showHide.bind('touch', function (ev) {
		showHide.parent().trigger('mouseover');
		showHide.show();
	}, function (ev) {
		showHide.parent().trigger('mouseover');
	});

});				 // ----- /SCOPE -----
