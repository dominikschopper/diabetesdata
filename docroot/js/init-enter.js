/**
 *
 */
require(['DDataConfig', 'toolkit', 'js/vendor/doT.js'], function(cfg, tk, dot) {
    console.log('cfg', cfg);
    var types = {
        sugar: {
            templateSelector: '#enter-sugar',
            call: showSugarForm
        },
        insulin: {
            templateSelector: '#enter-insulin',
            call: showInsulinForm
        },
        meal: {
            templateSelector: '#enter-meal',
            call: showMealForm
        },
        activity: {
            templateSelector: '#enter-activity',
            call: showMealForm
        },
        default: 'sugar'
    };

    function showForm(which, data) {
        which = which || types.default;
        data = data || {};
        if (!data.hasOwnProperty('date')) {
            data.date = Date.now();
        }
        // do we have a date or a string?
        if (data.date.constructor !== Date) {
            data.date = new Date(data.date);
        }
        var year = data.date.getFullYear(),
            month = (data.date.getMonth() + 1),
            mday = data.date.getDate(),
            hour = data.date.getHours(),
            min = data.date.getMinutes();
        mday = tk.leadingZeros(mday);
        month = tk.leadingZeros(month)
        hour = tk.leadingZeros(hour);
        min = tk.leadingZeros(min);
        data.dateString = year + '-' + month + '-' + mday;
        data.timeString = hour + ':' + min;
        // timezone offset to correct the mongodb GMT+0000
        data.tz = data.date.getTimezoneOffset() / 60;

        if (!data.hasOwnProperty("value")) {
            data.value = '';
        }
        if (!data.hasOwnProperty("measurement")) {
            data.measurement = "";
        }
        //console.log('which %o / data %o / insert %o', which, data, insertHere);
        var formTemplate = dot.template($(types[which].templateSelector).text());
        insertHere.empty().append(formTemplate(data));
        insertHere.find('[autofocus]').focus();
    }

    function getActiveType() {
    	var activeClass = '.enter-active';
    	var which = $(activeClass).data('type') || 'sugar';
    	return which;
    }

    $('.refresh-data').bind('click', function (ev) {
    	var which = getActiveType();
    	types[which].call({}, cfg);
    });

    function showSugarForm(data, cfg) {
        var which = 'sugar';
        if (!data.hasOwnProperty('value')) {
            data.value = 100;
        }
        if (!data.hasOwnProperty("measurement")) {
            data.measurement = cfg.measurements.sugar;
        }
        showForm(which, data);
    }

    function showMealForm(data, cfg) {
        var which = 'meal';
        if (!data.hasOwnProperty('value')) {
            data.value = 1;
        }
        if (!data.hasOwnProperty("measurement")) {
            data.measurement = cfg.measurements.meal;
        }
        showForm(which, data);
    }

    function showInsulinForm(data, cfg) {
        var which = 'insulin';
        var insArray = [];
        for (var i in cfg.insulin) {
            if (cfg.insulin.hasOwnProperty(i)) {
                insArray.push(i);
            }
        }
        data.insulin = insArray;
        data.value = 3;
        //console.log('INSULIN ARRAY', data.insulin);
        data.measurement = cfg.measurements.insulin;
        showForm(which, data);
    }

    function showActivityForm(data, cfg) {
        var which = 'activity';
        data.value = "";
        showForm(which, data);
    }
    var clickFuncs = {
        sugar: showSugarForm,
        insulin: showInsulinForm,
        meal: showMealForm,
        activity: showActivityForm
    };

    function markActiveButton(whichType, clickedButton) {
        var $buttons = $('.form-wrapper .enter-button');
        $buttons.removeClass('enter-active');
        $(clickedButton).addClass('enter-active');
    }
    var insertHere = $('#insert-form');
    showSugarForm({}, cfg);
    $('.enter-button').bind('click', function(ev) {
        //ev.stopPropagation();
        ev.preventDefault();
        var $button = $(this);
        var which = $button.data('type');
        var conf = cfg;
        if (!which || !clickFuncs.hasOwnProperty(which)) {
            console.warn('no type to show setting to "sugar"');
            which = 'sugar';
        }
        markActiveButton(which, $button);
        console.log('showing', which);
        var form = clickFuncs[which];
        form({}, conf);
    });
});
