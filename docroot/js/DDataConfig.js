/**
 *
 */

"use strict";

define({
	"insulin": {
		"Humalog": {
			"color": "#b9def0"
		},
		"Lantus": {
			"color": "rgba(121, 136, 212, 0.9)"
		}
	},
	"measurements": {
		"sugar": "mg/dl",
		"meal": "BE",
		"insulin": "ie"
	}
});