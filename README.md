# Diabetes Data

a small nodejs / express application that stores different types of data concerning diabetes like blood
sugar, insulin dosage, carbohydrates (meals) ...

since the data should be entered anywhere and anytime, this is per se an application for everyday use
and sp it has to be responsive with a strong focus on mobile input

the available data is personal and shall not be visible publically, so each user has a username/password pair.
userames are generally email adresses

in a future version the data shall be available to another user too

## thx to

- for the free icon set https://www.iconfinder.com/search/?q=iconset:Hand_Drawn_Web_Icon_Set
- jquery &amp; jqueryui
- of course nodejs, mongo, monooose and expressjs

## inputs

there are three main types of input

- each input measurement can be edited afterwards
- each input has an editable date and time field,

### input types

- bloodsugar (in mg/dl or mmol choosable in settings)<br />  input fields: amount, date, time

<nowiki><div class="field-examples">
<input type="date" value="2014-04-16" /> <input type="time" value="10:10" />
<br /><br />
<input type="number" min="0" max="500" step="1" value="100" placeholder="" width="" /> mg/dl
</div></nowiki>

- carbohydrates (meals)<br />
 input fields: amount, date, time

<nowiki><div class="field-examples">
<input type="date" value="2014-04-16" /> <input type="time" value="10:10" />
<br /><br />
<input type="number" min="0" max="500" step="1" value="100" placeholder="" width="" /> BE/KHE
</div></nowiki>


- insulin (types of insulin are shown in different colors and can be configured via settings - insulin)<br />
 input fields: amount, date, time

<nowiki><div class="field-examples">
<input type="date" value="2014-04-16" /> <input type="time" value="10:10" />
<br /><br />
<select name="insulins">
<option type="A">Humalog</option>
<option type="A">Lantus/Glargin</option>
</select>
<input type="number" min="1" max="100" step="1" value="100" placeholder="" /> ie
</div></nowiki>


## settings

all settings that can be stored

## user

user accounts can be freely created

each user account has a username (its email adress) a password and a visible name

<nowiki><div class="field-examples">
<label class="settings">
Username:
<input type="email" name="email" value="mail@dschopper.de" />
</label>

<label class="settings">
New Password:
<input type="password" name="password" placeholder="s3cr3t p4ssw0rd" />
</label>

<h4>insulin types 	<button title="Add Insulin" style="float:right">Add new</button></h4>

<div class="insulin-type">
<input type="text" name="insulin-a" value="Humalog" />
<input type="color" name="insulin-a-color" value="#1616a0" />
<div style="float: right" title="delete this insulin"><button class="enlarge-me">Del</button></div>
</div>

<div class="insulin-type">
<input type="text" name="insulin-b" value="Glargin/Lantus" />
<input type="color" name="insulin-b-color" value="#16a0a0" />
<div style="float: right" title="delete this insulin"><button class="enlarge-me">Del</button></div>
</div>

<h4>Insulin Units</h4>
<div class="insulin-type">
Show bloodsugar in
<label><input type="radio" name="sugar-units" value="mmol"> mmol/l</label>
<label><input type="radio" name="sugar-units" value="mg" checked> mg/dl</label>
</div>
</div></nowiki>

additionally a user can edit the bloodsugar measurement units (mg/dl of blood or mmol/l of blood) and the types of
insulin he wants to input (name and color).

in a future version the data entered can be accessible for other users (invitations can be sent to email addresses), so
e.g. a doctor can view the data that has been entered

## pages/views

### startup screen

viewable: `public`

login/subscribe page with login fields first and the subscribe fields visible if requested, link to info page

### info page

viewable: `public`

info about the project, contact

### list view

viewable: `owner|shared users`

lists all entered data

filters should be available for

- time (default: last 3 days)
- type of data (default all)

charts shall be shown (using nvd3 with d3) for the data

### enter data/edit data

viewable: `owner`

input field for bloodsugar


<style type="text/css">
.field-examples {width:25em;overflow:visible;padding:0.5em 1.5em;background-color:rgba(255,255,255,0.666);border:1px solid rgb(92,92,92);border-radius:0.5em;}
.insulin-type {margin: 0.5em 0;}
label.settings {display: block;margin: 0.5em 0;}
.enlarge-me {width: 2em;float: right;display:block;}
</style>
